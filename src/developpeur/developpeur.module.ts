import { Module } from "@nestjs/common";
import { DeveloppeurService } from "./developpeur.service";
import { DeveloppeurController } from "./developpeur.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { DeveloppeurSchema } from "./schema/developpeur.schema";

@Module({
  imports: [MongooseModule.forFeature([{ name: "Developpeur", schema: DeveloppeurSchema }])],
  providers: [DeveloppeurService],
  controllers: [DeveloppeurController]
})
export class DeveloppeurModule {
}
