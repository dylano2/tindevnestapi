import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose from "mongoose";
import { Langage } from "../../langage/schema/langage.schema";


@Schema({
  timestamps: true
})

export class Developpeur {


  @Prop()
  pseudo: string;

  @Prop()
  nom: string;

  @Prop()
  prenom: string;

  @Prop()
  password: string;

  @Prop({
    unique: [true, "Duplicate email"]
  })
  email: string;

  @Prop({ type: mongoose.Schema.Types.Array, ref: "Langage" })
  langages: Langage[];
}

export const DeveloppeurSchema = SchemaFactory.createForClass(Developpeur);