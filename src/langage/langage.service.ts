import { Injectable } from "@nestjs/common";
import { Langage } from "./schema/langage.schema";
import mongoose from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Query as ExpressQuery } from "express-serve-static-core";

@Injectable()
export class LangageService {
  constructor(@InjectModel(Langage.name) private langageModel: mongoose.Model<Langage>) {
  }
  async create(langage: Langage): Promise<Langage> {
    return this
      .langageModel
      .create(langage);
  }

  async findAll(query: ExpressQuery): Promise<Langage[]> {
    const resPerPage = 10;
    const currentPage = Number(
        query.page)
      ||
      0;

    const skip = resPerPage * (currentPage - 1);

    const keyword = query.keyword ?
      {
        nom: {
          $regex: query.keyword,
          $options: "i"
        }
      }
      :
      {};

    return this
      .langageModel
      .find({ ...keyword })
      .limit(currentPage ? resPerPage : null)
      .skip(skip >= 0 ? skip : null);
  }
}
