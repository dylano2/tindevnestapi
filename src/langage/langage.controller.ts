import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import { LangageService } from "./langage.service";
import { CreateLangageDto } from "./dto/create-langage.dto";
import { Langage } from "./schema/langage.schema";
import { Query as ExpressQuery } from "express-serve-static-core";

@Controller("langage")
export class LangageController {

  constructor(private readonly langageService: LangageService) {
  }

  @Get("all")
  async getAllLangages(@Query() query: ExpressQuery): Promise<Langage[]> {
    return this.langageService.findAll(query);
  }

  @Post("create")
  async createLangage(@Body() langage: CreateLangageDto): Promise<Langage> {
    return this.langageService.create(langage);
  }


}
