import { Langage } from "../../langage/schema/langage.schema";

export class CreateDevDto{
  
  readonly pseudo: string;

  readonly nom: string;

  readonly prenom:string;

  readonly email: string;

  readonly password: string;

  readonly langages: Langage[];

}