import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import { DeveloppeurService } from "./developpeur.service";
import { Query as ExpressQuery } from "express-serve-static-core";
import { Developpeur } from "./schema/developpeur.schema";
import { CreateDevDto } from "./dto/create-dev.dto";


@Controller("dev")
export class DeveloppeurController {

  constructor(private readonly developpeurService: DeveloppeurService) {
  }


  @Post("create")
  async createDev(
    @Body()
      dev : CreateDevDto
  ): Promise<Developpeur> {

    return this.developpeurService.create(dev);
  }

  @Get("all")
  async getAllDevs(
    @Query()
      query:
      ExpressQuery
  ): Promise<Developpeur[]> {

    const devs = await this.developpeurService.findAll(query)
    console.log(devs.length);
    return devs;
  }
}
