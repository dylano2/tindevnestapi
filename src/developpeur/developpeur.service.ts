import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Developpeur} from "./schema/developpeur.schema";
import mongoose from "mongoose";

import {Query as ExpressQuery} from "express-serve-static-core";


@Injectable()
export class DeveloppeurService {
    constructor(@InjectModel(Developpeur.name) private developpeurModel: mongoose.Model<Developpeur>) {
    }


    async findAll(query: ExpressQuery): Promise<Developpeur[]> {
        const resPerPage = 12;
        const currentPage = Number(
                query.page)
            ||
            0;

        const skip = resPerPage * (currentPage - 1);


        const keyword = query.keyword ?
            {
                pseudo: {
                    $regex: query.keyword,
                    $options: "i"
                }
            }
            :
            {};

        return this.developpeurModel
            .find({...keyword})
            .limit(currentPage ? resPerPage : null)
            .skip(skip >= 0 ? skip : null);
    }


    async create(dev: Developpeur): Promise<Developpeur> {
        return await this.developpeurModel.create(dev);
    }


}
