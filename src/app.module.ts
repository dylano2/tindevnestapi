import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import * as process from "process";
import { ConfigModule } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";
import { DeveloppeurModule } from './developpeur/developpeur.module';
import { LangageController } from './langage/langage.controller';
import { LangageModule } from './langage/langage.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ".env",
      isGlobal: true
    }),
    MongooseModule.forRoot(process.env.DB_URI,{
      autoIndex: true
    }),
    DeveloppeurModule,

    LangageModule,

    ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
