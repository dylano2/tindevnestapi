import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose from "mongoose";


@Schema({
  timestamps: true
})

export class Langage {
  @Prop({
    unique: true
  })
  nom: string;

  @Prop()
  isFramework: boolean;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: "Langage" })
  techno?: Langage;
}

export const LangageSchema = SchemaFactory.createForClass(Langage);