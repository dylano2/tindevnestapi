import { Langage } from "../schema/langage.schema";

export class CreateLangageDto {

  readonly nom: string;

  readonly isFramework: boolean;

  readonly techno: Langage;

}