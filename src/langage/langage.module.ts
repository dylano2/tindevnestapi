import { Module } from '@nestjs/common';
import { LangageController } from './langage.controller';
import { LangageService } from './langage.service';
import { MongooseModule } from "@nestjs/mongoose";
import { LangageSchema } from "./schema/langage.schema";

@Module({
  imports: [MongooseModule.forFeature([{ name: "Langage", schema: LangageSchema }])],
  controllers: [LangageController],
  providers: [LangageService]
})
export class LangageModule {}
